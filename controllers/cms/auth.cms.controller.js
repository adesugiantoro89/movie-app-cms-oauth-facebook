const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { User } = require("../../models");
const cloudinary = require("../../services/cloudinary.service");
const Op = require("sequelize").Op;
const fs = require("fs");
require("dotenv").config();
const role = { superAdmin: 1, admin: 2 };

class AuthController {
  static async login(req, res, next) {
    try {
      let token;
      if (req.body.email && req.body.password) {
        const user = await User.findOne({
          where: { email: req.body.email, [Op.not]: [{ roleId: 3 }] },
        });
        if (!user) {
          throw {
            status: 401,
            message: "Invalid email or password!",
          };
        }
        if (!bcrypt.compareSync(req.body.password, user.password)) {
          throw {
            status: 401,
            message: "Invalid email or password",
          };
        }

        token = jwt.sign(
          { id: user.id, roleId: user.roleId, email: user.email },
          process.env.JWT_SECRET_KEY
          // { expiresIn: "1h" }
        );
      } else {
        throw {
          status: 404,
          message: "Invalid email or password",
        };
      }
      res.status(200).json({ token });
    } catch (error) {
      next(error);
    }
  }

  static async register(req, res, next) {
    try {
      const adminRoleId = 2;

      console.log(req.file);
      const user = await User.findOne({
        where: {
          email: req.body.email,
          auth: "local",
        },
      });
      if (user) {
        throw {
          status: 409,
          message: "User already exist",
        };
      }

      req.body.password = bcrypt.hashSync(req.body.password, 10);
      req.body.auth = "local";
      req.body.roleId = adminRoleId;

      if (req.file) {
        // const fullPath = req.file.path.split("\\");
        // req.body.photo_profile = `http://localhost:3000/${fullPath[2]}`;
        const cdn = await cloudinary.uploader.upload(req.file.path);
        req.body.photo_profile = cdn.secure_url;
        // fs.unlinkSync(req.file.path);
      }

      User.create(req.body);
      res.status(201).json({ message: "Successfully register user" });
    } catch (error) {
      next(error);
    }
  }

  static async authorizationSuperAdmin(req, res, next) {
    try {
      //cek auth token
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }

      //cek payload
      const payload = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET_KEY
      );
      if (!payload) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }

      //cek user role superadmin
      const loggedInUser = await User.findOne({
        where: { email: payload.email, roleId: role.superAdmin },
      });
      if (!loggedInUser) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }
      req.user = payload;
      next();
    } catch (error) {
      next(error);
    }
  }

  static async authorizationAdmin(req, res, next) {
    try {
      //cek auth token
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized request a",
        };
      }

      //cek payload
      const payload = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET_KEY
      );
      console.log(payload);
      if (!payload) {
        throw {
          status: 401,
          message: "Unauthorized request b",
        };
      }

      //cek user role admin
      const loggedInUser = await User.findOne({
        where: {
          email: payload.email,
          [Op.not]: [{ roleId: 3 }],
        },
      });
      if (!loggedInUser) {
        throw {
          status: 401,
          message: "Unauthorized request c",
        };
      }
      req.user = payload;
      next();
    } catch (error) {
      next(error);
    }
  }
}
module.exports = AuthController;
