const { Movie, User } = require("../../models");
const cloudinary = require("../../services/cloudinary.service");
const fs = require("fs");
class MovieController {
  static async getAll(req, res, next) {
    try {
      let where = {};
      if (req.user.roleId === 2) {
        where = { userId: req.user.id };
      }
      const movies = await Movie.findAll({
        where: { userId: req.user.id },
        attributes: ["id", "name", "show_time"],
      });
      res.status(200).json({ movies });
    } catch (error) {
      next(error);
    }
  }

  static async getById(req, res, next) {
    try {
      console.log(req.file);
      const movie = await Movie.findOne({
        where: { id: req.params.id, userId: req.user.id },
      });
      if (!movie) {
        throw {
          status: 404,
          message: "Movie Not Found",
        };
      }
      res.status(200).json(movie);
    } catch (error) {
      next(error);
    }
  }

  static async add(req, res, next) {
    try {
      if (req.file) {
       
        const cdn = await cloudinary.uploader.upload(req.file.path);
        req.body.poster = cdn.secure_url;
     
      }
      req.body.userId = req.user.id;
      await Movie.create(req.body);
      res.status(201).json({ message: "Successfully create movie" });
    } catch (error) {
      next(error);
    }
  }

  static async update(req, res, next) {
    try {
      console.log(req.file);
      const movie = await Movie.findOne({
        where: { id: req.params.id, userId: req.user.id },
      });
      if (!movie) {
        throw {
          status: 404,
          message: "Movie Not Found",
        };
      }
      if (req.file) {
     
        const cdn = await cloudinary.uploader.upload(req.file.path);
        req.body.poster = cdn.secure_url;
       
      }
      movie.poster = req.body.poster || movie.poster;
      movie.name = req.body.name || movie.name;
      movie.show_time = req.body.show_time || movie.show_time;
      await movie.save();
      res.status(200).json({ message: "Successfully update movie" });
    } catch (error) {
      next(error);
    }
  }

  static async delete(req, res, next) {
    try {
      const movie = await Movie.findOne({
        where: { id: req.params.id, userId: req.user.id },
      });
      if (!movie) {
        throw {
          status: 404,
          message: "Movie Not Found",
        };
      }
      await movie.destroy();
      res.status(200).json({ message: "Successfully delete movie" });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = MovieController;
