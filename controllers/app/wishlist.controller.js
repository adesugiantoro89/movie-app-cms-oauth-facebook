const { Wishlist, User, Movie } = require("../../models");

class WishlistController {
  static async add(req, res, next) {
    try {
      req.body.userId = req.user.id;
      await Wishlist.create(req.body);
      res.status(201).json({ message: "Successfully add wishlist" });
    } catch (error) {
      next(error);
    }
  }

  static async get(req, res, next) {
    try {
      const wishlists = await Wishlist.findAll({
        where: { userId: req.user.id },
        attributes: [],
        include: [
          {
            model: User,
            as: "User",
            attributes: ["email", "auth"],
          },
          {
            model: Movie,
            as: "Movie",
            attributes: ["name", "show_time"],
          },
        ],
      });
      res.status(200).json({ wishlists });
    } catch (error) {
      next(error);
    }
  }

  static async delete(req, res, next) {
    try {
      const wishlist = await Wishlist.findOne({
        where: { id: req.params.id },
      });
      if (!wishlist) {
        throw {
          status: 404,
          message: "Wishlist Not Found",
        };
      }

      if (wishlist.userId !== req.user.id) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }

      await wishlist.destroy();
      res.status(200).json({ message: "Successfully delete wishlist" });
    } catch (error) {
      next(error);
    }
  }
}
module.exports = WishlistController;
