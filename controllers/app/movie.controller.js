const { Movie } = require("../../models");

class MovieController {
  static async moviesToBeShown(req, res, next) {
    const movies = await Movie.findAll({ attributes: ["name", "show_time"] });
    res.status(200).json(movies);
  }
}
module.exports = MovieController;
