const fs = require("fs");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { User } = require("../../models");
const { OAuth2Client } = require("google-auth-library");
const axios = require("axios");
const client = new OAuth2Client(
  process.env.GOOGLE_CLIENT_ID,
  process.env.GOOGLE_CLIENT_SECRET
);
const cloudinary = require("../../services/cloudinary.service");
require("dotenv").config();

class AuthController {
  static async login(req, res, next) {
    try {
      let token;
      const userRoleId = 3;

      if (req.body.email && req.body.password) {
        const user = await User.findOne({ where: { email: req.body.email } });
        if (!user) {
          throw {
            status: 401,
            message: "Invalid email or password!",
          };
        }
        if (!bcrypt.compareSync(req.body.password, user.password)) {
          throw {
            status: 401,
            message: "Invalid email or password",
          };
        }

        token = jwt.sign(
          { id: user.id, roleId: user.roleId, email: user.email },
          process.env.JWT_SECRET_KEY
          // { expiresIn: "1h" }
        );

        //google
      } else if (req.body.google_id_token) {
        const payload = await client.verifyIdToken({
          idToken: req.body.google_id_token,
          audiance: process.env.GOOGLE_CLIENT_IDs,
        });

        const user = await User.findOne({
          where: { email: payload.payload.email, auth: "google" },
        });
        if (user) {
          token = jwt.sign(
            { id: user.id, roleId: user.roleId, email: user.email },
            process.env.JWT_SECRET_KEY
          );
        } else {
          const createdUser = await User.create({
            roleId: userRoleId,
            email: payload.payload.email,
            auth: "google",
          });
          token = jwt.sign(
            {
              id: createdUser.id,
              roleId: createdUser.roleId,
              email: createdUser.email,
            },
            process.env.JWT_SECRET_KEY
          );
        }

        //facebook
      } else if (req.body.facebook_id_token) {
        const url =
          "https://graph.facebook.com/v12.0/me?fields=id%2Cname%2Cemail%2Cgender%2Cbirthday&access_token=" +
          req.body.facebook_id_token;

        const response = await axios.get(url);

        const user = await User.findOne({
          where: { email: response.data.email, auth: "facebook" },
        });
        if (user) {
          token = jwt.sign(
            { id: user.id, roleId: user.roleId, email: user.email },
            process.env.JWT_SECRET_KEY
          );
        } else {
          const createdUser = await User.create({
            roleId: userRoleId,
            email: response.data.email,
            auth: "facebook",
          });

          token = jwt.sign(
            {
              id: createdUser.id,
              roleId: createdUser.roleId,
              email: createdUser.email,
            },
            process.env.JWT_SECRET_KEY
          );
        }
      } else {
        throw {
          status: 404,
          message: "Invalid email or password",
        };
      }
      res.status(200).json({ token });
    } catch (error) {
      next(error);
    }
  }

  static async register(req, res, next) {
    try {
      console.log(req.file);

      const userRoleId = 3;
      const user = await User.findOne({
        where: {
          email: req.body.email,
          auth: "local",
        },
      });
      // res.send(user);
      if (user) {
        throw {
          status: 409,
          message: "User already exist",
        };
      }

      if (req.file) {
        // const fullPath = req.file.path.split("\\");
        // req.body.photo_profile = `http://localhost:3000/${fullPath[2]}`;
        const cdn = await cloudinary.uploader.upload(req.file.path);
        req.body.photo_profile = cdn.secure_url;
        // fs.unlinkSync(req.file.path);
      }

      req.body.password = bcrypt.hashSync(req.body.password, 10);
      req.body.auth = "local";
      req.body.roleId = userRoleId;
      User.create(req.body);
      res.status(201).json({ message: "Successfully register user" });
    } catch (error) {
      next(error);
    }
  }

  static async authorization(req, res, next) {
    try {
      //cek auth token
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }

      //cek payload
      const payload = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET_KEY
      );
      if (!payload) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }

      //cek user
      const loggedInUser = await User.findOne({
        where: { email: payload.email },
      });
      if (!loggedInUser) {
        throw {
          status: 401,
          message: "Unauthorized request",
        };
      }
      req.user = payload;
      next();
    } catch (error) {
      next(error);
    }
  }
}
module.exports = AuthController;
