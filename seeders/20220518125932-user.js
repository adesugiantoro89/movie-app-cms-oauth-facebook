"use strict";
const bcrypt = require("bcryptjs");

module.exports = {
  async up(queryInterface, Sequelize) {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync("1234567890", salt);
    await queryInterface.bulkInsert("Users", [
      {
        roleId: 1,
        email: "superadmin@gmail.com",
        password: hash,
        auth: "local",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        roleId: 2,
        email: "admin@gmail.com",
        password: hash,
        auth: "local",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        roleId: 3,
        email: "adesugiantoro89@gmail.com",
        password: hash,
        auth: "local",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    queryInterface.bulkDelete("Users", null, {
      truncate: true,
      restartIdentity: true,
    });
  },
};
