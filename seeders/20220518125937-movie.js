"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert("Movies", [
      {
        userId: 1,
        name: "Doctor Strange : Multiverse Of Madness",
        show_time: "2022-18-22",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        userId: 1,
        name: "KKN Desa Penari UNCUT",
        show_time: "2022-18-22",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        userId: 1,
        name: "Gara-gara Warisan",
        show_time: "2022-18-22",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        userId: 1,
        name: "Kuntilanak 3",
        show_time: "2022-18-22",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        userId: 2,
        name: "FireStarter",
        show_time: "2022-18-22",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Movies", null, {
      truncate: true,
      restartIdentity: true,
    });
  },
};
