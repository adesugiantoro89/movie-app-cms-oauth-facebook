const { body, check, validationResult } = require("express-validator");
const AuthController = require("../../controllers/app/auth.app.controller");
const WishlistController = require("../../controllers/app/wishlist.controller");

const router = require("express").Router();
router.post(
  "/",
  AuthController.authorization,
  [check("movieId", "movieId is required").not().isEmpty()],
  (req, res, next) => {
    let errors = validationResult(req);
    if (!errors.isEmpty()) {
      next({
        status: 400,
        message: errors.errors[0].msg,
      });
    }
    next();
  },
  WishlistController.add
);
router.get("/", AuthController.authorization, WishlistController.get);
router.delete("/:id", AuthController.authorization, WishlistController.delete);

module.exports = router;
