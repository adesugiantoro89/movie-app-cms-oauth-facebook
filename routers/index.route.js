const router = require("express").Router();
const appRoutes = require("./app/index.app.route");
const cmsRoutes = require("./cms/index.cms.route");

router.use("/app", appRoutes);
router.use("/cms", cmsRoutes);

module.exports = router;
